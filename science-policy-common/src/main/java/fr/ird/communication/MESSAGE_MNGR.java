package fr.ird.communication;

import java.util.*;

import fr.ird.strings.DELIMITERS;


public class MESSAGE_MNGR{
	//	--OK-------------------------------------------------------------------------
	public MESSAGE_MNGR() {}
	//	--OK-------------------------------------------------------------------------
	public static String first(String message){
		String [] parsed = parseMessage(message);
		return(parsed[0]);
	}
	//	---------------------------------------------------------------------------
	public static String listToString(int month, double[] list){
		StringBuffer buffer = new StringBuffer(month + DELIMITERS.seoln);
		for (int i = 0; i < list.length; i++) {	
			buffer.append (list[i] + DELIMITERS.seoln);
			};
		return(buffer.toString());
		}
	//	---------------------------------------------------------------------------
	public static double[] stringToList(String all){
		String [] parsed = parseMessage(all);
		// int month = Integer.parseInt(parsed[0]);
		int sz = parsed.length;
		double [] list = new double[sz];
		for(int s=0;s<sz-1;s++){
			list[s] = Double.parseDouble(parsed[s+1]);
		}
		return(list);
	}
	//	--OK-------------------------------------------------------------------------
	public static String vectorToString(Vector<String[]> vector, String identity){
		StringBuffer buffer = new StringBuffer("");
		for (int i = 0; i < vector.size(); i++){	
			String[] st = vector.elementAt(i);
			if(st[1].equals(identity) || st[3].equals(identity)) {
				for(int k=0; k < 5; k++) {
					if( k !=0 || k !=2)
						buffer.append (st[k] + DELIMITERS.seoln);
					}
				}
			};
		return(buffer.toString());
	}
	//	---------------------------------------------------------------------------
	public static Vector<String[]> stringToVector(String all){
		Vector<String[]> vector = new Vector<String[]>();
		String [] parsed = parseMessage(all);
		int sz = parsed.length;
		int nl = sz/3;
		for(int l=0;l<nl;l++){
			String[] st = new String[4];
			for(int s=0;s<3;s++) st[s] = parsed[3 * l +s];
			vector.addElement(st);
		}
		return(vector);
	}
	// --------------------------------------------------------------------------
	public static  String [] parseMessage(String msg){
		StringTokenizer st = new StringTokenizer(msg, DELIMITERS.seoln);
		String [] all = new String[5000];
		int nb = 0;
		while(st.hasMoreTokens()){
			all[nb] = st.nextToken();
			nb ++;
		}
		String[] parse = new String[nb];
		for(int n=0;n<nb;n++) parse[n] = all[n];
		return(parse);
	}
	//	--------------------------------------------------------------------------------------
	public static String cod(String s){   return(s.replaceAll(DELIMITERS.seoln," --- "));  }
	//	---------------------------------------------------------------------------
	public static String decod(String s){  return(s.replaceAll(" --- ",DELIMITERS.seoln));  }
	//	--------------------------------------------------------------------------------------
}
