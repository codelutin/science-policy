package fr.ird.model;

import java.util.*;

import fr.ird.communication.MESSAGE_MNGR;


public class PLAY {
	static Vector<String[]> allMessages;
	static int numPlay;
	static boolean running;
	OCEAN ocean;
	// ---------------------------------------------------------------------------
	public PLAY() {
		allMessages = new Vector<String[]>();
		running = false;
		ocean = new OCEAN(24);
		System.out.println("ocean");
	}  
	//---------------------------------------------------------------------------
	public void init(int npl){
		numPlay = npl;
		}
	//---------------------------------------------------------------------------
	public void start(){
		running = true;
		ocean.start();
		}
	//---------------------------------------------------------------------------
	public String processMessage(String fromMessage){
		String[] fMessage = MESSAGE_MNGR.parseMessage(MESSAGE_MNGR.decod(fromMessage));
		if(running) update();
		String rMessage = "OTHER";
		String identity = fMessage[1];
		String typeMessage = fMessage[2];
		if(typeMessage.equals("MSG")) rMessage = addMessage(fMessage);
		if(typeMessage.equals("MSG?")) rMessage = getMessages(identity);
		if(typeMessage.equals("VIEW?")) rMessage = getView();
		if(typeMessage.equals("ACT")) rMessage = act(fMessage);
		return(MESSAGE_MNGR.decod(rMessage));
		}
	//---------------------------------------------------------------------------
	String getView(){
		return(MESSAGE_MNGR.listToString(ocean.month,ocean.getValues()));
		}
	//---------------------------------------------------------------------------
	String getMessages(String identity){
		if(ocean.endSimu()) GAME.endAPlay(numPlay);
		if(running) return(MESSAGE_MNGR.vectorToString(allMessages,identity));
		else return("NOT_RUNNING");
		}
	//---------------------------------------------------------------------------
	String addMessage(String[] fMessage){
		allMessages.add(fMessage);
		return("RECEIVED");
		}
	//---------------------------------------------------------------------------
 	void update(){
		ocean.update();
 		}
	//---------------------------------------------------------------------------
 	String act(String[] fMessage){
 		ocean.update(Integer.parseInt(fMessage[3]));
		return("ACT");
 		}
	//---------------------------------------------------------------------------
 	}


