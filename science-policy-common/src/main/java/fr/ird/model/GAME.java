package fr.ird.model;

import fr.ird.communication.MESSAGE_MNGR;
import fr.ird.strings.MESSAGES;
import fr.ird.strings.DELIMITERS;


public class GAME {
	int nbPlayers, nbPlays, numPlay;
	public static PLAY[] thePlays;  
	// ---------------------------------------------------------------------------
	public GAME() {
		nbPlays = 0;
		nbPlayers = 0;
		thePlays = new PLAY[10];
		for(int i=0;i<10;i++) thePlays[i] = new PLAY();
		}
	//---------------------------------------------------------------------------
	public String processMessage(String fromMessage){
		String fMessage = MESSAGE_MNGR.first(MESSAGE_MNGR.decod(fromMessage));
		String rMessage = "OTHER";
		if(fMessage.equals("CONNECT")) rMessage = connectPlayer();
		else {
			numPlay = Integer.parseInt(fMessage);
			rMessage = thePlays[numPlay].processMessage(fromMessage);
		}
		return(MESSAGE_MNGR.decod(rMessage));
		}

	//---------------------------------------------------------------------------
	String connectPlayer(){
		String name = MESSAGES.playersNames[nbPlayers %3];
		if(nbPlayers % 3 == 0) thePlays[nbPlays].init(nbPlays);
		nbPlayers ++;
		numPlay = nbPlays;
		if(nbPlayers % 3 == 2) {
			thePlays[nbPlays].start();
			nbPlays++;
		}
		OCEAN ocean = thePlays[nbPlays].ocean;
		int EMSY = (int)(ocean.getEMSY() * OCEAN.mRnd(0.5));
		int YMSY = (int)(ocean.getYMSY() * OCEAN.mRnd(0.5));
		return("IDENTITY"
				+ DELIMITERS.seoln + numPlay 
				+ DELIMITERS.seoln + name
				+ DELIMITERS.seoln + EMSY
				+ DELIMITERS.seoln + YMSY);
		}
	//---------------------------------------------------------------------------
 	public static void endAPlay(int nPlay){
 		}
 	}


