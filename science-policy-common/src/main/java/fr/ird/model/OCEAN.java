package fr.ird.model;

public class OCEAN {
	double[] theStocks, theCatches, theEfforts, theTemps, thePrices, theIncomes;
	double q = 0.0004, r = 0.2, K = 2000.0f, 
			temp0 = 12.0f, 
			priceP = 100.0f, priceY = 100.0f, priceFlexibiliy = -1.0f,
			costE = 20.0f, costY = 5.0f;
	int month, fMonth, nbMonths;
	double effortValue, effortChange = 1.0f;
	long time, timeInit;
	boolean started;
	//---------------------------------------------------------------------------
	public OCEAN(int initMonth){
		nbMonths = 120;
		theStocks =  new double [nbMonths];
		theCatches =  new double [nbMonths];
		theEfforts =  new double [nbMonths];
		theTemps =  new double [nbMonths];
		thePrices =  new double [nbMonths];
		theIncomes =  new double [nbMonths];
		for(int m = 0; m<nbMonths; m++) {
			theStocks[m]= 0.0f;
			theCatches[m]= 0.0f;
			theEfforts[m]= 0.0f;
			theTemps[m] = 0.0f;
			thePrices[m] =  0.0f;
			theIncomes[m] =  0.0f;
			}
		theStocks[0] = mRnd(0.5f) * K / 2.0f;
		theTemps[0] = temp0;
		effortValue =  mRnd(0.5f) * r / (2 * q);
		theEfforts[0] = effortValue ;
		theCatches[0] = yFunction(theStocks[0], theEfforts[0]);
		thePrices[0] =  priceFunction(theCatches[0]);
		theIncomes[0] =  incomeFunction(theCatches[0], theEfforts[0], thePrices[0]);
		for(int m = 1; m < initMonth; m++) dynamics(m);
		month = initMonth;
		fMonth = initMonth;
		started = false;
	}
	//---------------------------------------------------------------------------
	void dynamics(int m){
		theTemps[m] = tempFunction(m);
		effortValue *= mRnd(0.2f);
		theEfforts[m]= effortValue;
		theStocks[m]= rFunction(theStocks[m-1], theTemps[m]) - theCatches[m-1];
		theCatches[m] = yFunction(theStocks[m], theEfforts[m]);
		thePrices[m] =  priceFunction(theCatches[m]);
		theIncomes[m] =  incomeFunction(theCatches[m], theEfforts[m], thePrices[m]);
	}
	//---------------------------------------------------------------------------
	public static double mRnd(double e){ return( 1.0f -e + 2.0f * e * Math.random());}
	//---------------------------------------------------------------------------
	public int getMaxS(){return((int)K);}
	//---------------------------------------------------------------------------
	public int getMaxY(){return((int)(K*r));}
	//---------------------------------------------------------------------------
	public int getMaxE(){return((int)(r/(2*q)));}
	//---------------------------------------------------------------------------
	public int getEMSY(){return((int)(r/(2*q)));}
	//---------------------------------------------------------------------------
	public int getYMSY(){return((int)(K * r / 4.0f));}
	//---------------------------------------------------------------------------
	public double getQ(){return(q);}
	//---------------------------------------------------------------------------
	public void start(){
		started = true;
		timeInit = System.currentTimeMillis();
		}
	//---------------------------------------------------------------------------
	double tempFunction(int m){
		double xm = (double)m;
		double tm = (temp0 * mRnd(0.2f) * (1.0f + (0.2f * xm)/nbMonths) + 0.2f * Math.sin(2.0f * xm * Math.PI/12.0f));
		return (tm);
		}
	//---------------------------------------------------------------------------
	double rFunction(double S, double temp){
		return (Math.max(0.0f, mRnd(0.3f) * (S + r * S *(1-S/K) )));
		}
	//---------------------------------------------------------------------------
	double yFunction(double S, double E){
		return (q * mRnd(0.2f) * S * E);
		}
	//---------------------------------------------------------------------------
	double priceFunction(double Y){
		if(Y < 0.001) return(0);
		return ( Math.pow( (Y/priceY), priceFlexibiliy)  * priceP * mRnd(0.2f) );
		}
	//---------------------------------------------------------------------------
	double incomeFunction(double Y, double E, double P){
		return (  mRnd(0.1f) * (Y * P - Y * costY - E*costE) );
		}
	//---------------------------------------------------------------------------
	double[] getValues(){
			double[] seq = new double[5*nbMonths+4];
			for(int m=0;m< nbMonths; m++){
				seq[m]= theCatches[m];
				seq[m+nbMonths]= theEfforts[m];
				seq[m+2*nbMonths]= theTemps[m];
				seq[m+3*nbMonths]= thePrices[m];
				seq[m+4*nbMonths]= theIncomes[m];
			}
			seq[5*nbMonths]= getQ();
			seq[5*nbMonths+1]= getMaxY();
			seq[5*nbMonths+2]= getMaxE();
			seq[5*nbMonths+3]= getMaxS();
			return (seq);
			}
	//---------------------------------------------------------------------------
	public String getMonth(){
		if(started) return (""+month);
		else return(""+fMonth);
		}
	//---------------------------------------------------------------------------
	public int setMonth(){
		time = System.currentTimeMillis();
		int iMonth = fMonth + (int)( (time-timeInit)/2000.0f);
		if(iMonth>nbMonths-1) iMonth = nbMonths-1;
		if(iMonth != month) {
			for(int m = month; m < iMonth; m++) dynamics(m);
		}
		return(iMonth);
	}
	//---------------------------------------------------------------------------
	public void update(){
		int iMonth = setMonth();
		month = iMonth;
	}
	//---------------------------------------------------------------------------
	public void update(int value){
		effortChange =  mRnd(0.2f) * (1.0f + value * 0.01f);
	}
	//---------------------------------------------------------------------------
	public boolean endSimu(){
		if(month == (nbMonths-1)) return(true);
		return(false);
	}
	//---------------------------------------------------------------------------
	public double[][] getVectors(){
		return (new double[][]{theStocks, theCatches, theEfforts, theTemps, thePrices, theIncomes});
		}
	//---------------------------------------------------------------------------
}
