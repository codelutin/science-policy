package fr.ird.strings;

import java.util.Arrays;

public class MESSAGES {

	
	public static String[] playersNames = 
			new String[]{"Scientist","Manager","Third part"};

	private static String[] messagesAB = new String[]{
"Limitations are alright","More limitations","Less limitations", 
"I am not certain", "More limitations","Less limitations", "Yes",  
"This is probable", "I have no certainty","Act quickly","I am not paid to answer that question"};
	private static String[] messagesAC = new String[]{
"Situation is good","Situation is bad","Manager is doing wrong",
"I have told to manager I was not certain"};
	private static String[] messagesBA = new String[]{
"Should I augment limitations?","How much?","What do you mean by uncertain?"};
	private static String[] messagesBC = new String[]{"I did right","I did wong", "I could not imagine what happened","Scientist is very useful", "You should augment science budget","Science  has no utility"};
	private static String[] messagesCA = new String[]{"Is it going wrong?"," Manager complains: I will cut your budget","Manager would like more certainty"};
	private static String[] messagesCB = new String[]{"I want a short term increase of production","I want a short term increase of production","You are doing well", "You are doing wrong","Change your strategy"};
	private static String[] common = new String[]{"Yes","No","Maybe","A little","Much","More","How much?","-5%","-2%","+2%","+5%"};
	
	private static String stRnd(){
		return(""+(int)(10000.0f + 80000.0f * Math.random())+" ");
	}	
	public static String[] ret(String[] msg){
		String[] join = new String[msg.length+common.length];
		for(int c=0;c< msg.length;c++)join[c]=stRnd() + msg[c];
		for(int c=0;c< common.length;c++)join[c+msg.length]= stRnd() + common[c];
		Arrays.sort(join);
		for(int c=0;c< join.length;c++){
			join[c]=join[c].substring(6);
		}
		return(join);
	}

	public static String[] gMessagesAB = ret(messagesAB);
	public static String[] gMessagesAC = ret(messagesAC);
	public static String[] gMessagesBA = ret(messagesBA);
	public static String[] gMessagesBC = ret(messagesBC);
	public static String[] gMessagesCA = ret(messagesCA);
	public static String[] gMessagesCB = ret(messagesCB);
}
