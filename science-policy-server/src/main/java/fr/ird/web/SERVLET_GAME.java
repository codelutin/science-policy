package fr.ird.web;

import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OutputStream;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import fr.ird.configuration.CONFIGURATION;
import fr.ird.model.GAME;

// import game.MESSAGE_MNGR;	

public class SERVLET_GAME extends HttpServlet {
	private static final long serialVersionUID = 1L;
	GAME game;
	static String ctxt = "c";
	
	public CONFIGURATION configuration = CONFIGURATION.INSTANCE;
	
	public SERVLET_GAME() {
		super();
		System.out.println("Game");
		game = new GAME();
	}
	// -----------------------------------------------------------------------
	public void init(){
		try{
			ctxt = getServletConfig().getServletContext().getRealPath("/");
			ctxt = getServletContext().getRealPath("/");
			System.out.println(ctxt);
		}
		catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	// -----------------------------------------------------------------------
	public void doGet(
			HttpServletRequest request,
			HttpServletResponse response) throws ServletException, IOException {
		doPost(request, response);
	}

	// -----------------------------------------------------------------------
	public void doPost(	HttpServletRequest request,	HttpServletResponse response) throws ServletException, IOException {
		String toPlayer = "Depuis servlet";
		try {
			response.setContentType("application/x-java-serialized-object");
			InputStream in = request.getInputStream();
			ObjectInputStream inputFromApplet = new ObjectInputStream(in);
			String fromPlayer = (String) inputFromApplet.readObject(); 			
			if(configuration.isVerbose())
				System.out.println("THE_GAME ----------------  FROM :    "+ fromPlayer);
			synchronized( this ){
				// traitement du message recu de l'applet par le programme "THE_GAME.processMessage"
				toPlayer = game.processMessage(fromPlayer);
			}
			if(configuration.isVerbose())
				System.out.println("THE_GAME ------------------  TO :     "+ toPlayer);
			OutputStream outstr = response.getOutputStream();
			ObjectOutputStream oos = new ObjectOutputStream(outstr);
			oos.writeObject(toPlayer);
			oos.flush();
			oos.close();
		} catch (Exception e) {
			e.printStackTrace();
			System.exit(0);
		}
	}
	// -----------------------------------------------------------------------
}
