package fr.ird.configuration;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public enum CONFIGURATION {
    INSTANCE;

    private Properties props;

    private CONFIGURATION() {
        this.props = new Properties();
        InputStream inStream = CONFIGURATION.class.getResourceAsStream("/configuration.properties");
        try {
            this.props.load(inStream);
            
        } catch (IOException e) {
            System.err.println("[Configuration] - couldn't read configuration file : " + e.getMessage());
        }
    }
    
    public boolean isVerbose() {
        return Boolean.parseBoolean(this.props.getProperty("verbose"));
    }
}
