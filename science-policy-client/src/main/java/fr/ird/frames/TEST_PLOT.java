package fr.ird.frames;

import java.applet.Applet;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Toolkit;

import fr.ird.model.OCEAN;


public class TEST_PLOT extends Applet  {
	// ----------------------------------------------------------------------------
	// ----------------------------------------------------------------------------
	static final long serialVersionUID = -1L;
	double[] theStocks, theCatches , theEfforts , theTemps , thePrices , theIncomes;
	
	// -----------------------------------------------------------------------------
	public void init() {
		setSize(500, 500);
		OCEAN ocean = new OCEAN(120);
		double [][] values = ocean.getVectors();
		theStocks = values[0];
		theCatches = values[1];
		theEfforts = values[2];
		theTemps = values[3];
		thePrices = values[4];
		theIncomes  = values[5];
		// -----------------------------------
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		validate();
		int ww = (int)(0.6f * screenSize.width);
		int hh = (int)(0.6f * screenSize.height);
		setSize(ww,hh);
		setLayout(new GridBagLayout ()); 
	    // -----------------------------------------------------------------------------
		// -----------------------------------
		GridBagConstraints gbc = new GridBagConstraints();
        gbc.fill = GridBagConstraints.BOTH;
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.weightx = 1.0f;
        gbc.weighty = 0.1f;
        PLOT plot = new PLOT("test");
		System.out.println("this "+ this.getWidth()+ " "+ this.getHeight());
		add(plot,gbc);
		System.out.println("plot "+ plot.getWidth()+ " "+ plot.getHeight());
		// plot.reset(1, 120, theIncomes, null, "","theIncomes", 10, 10, "test");
		// plot.reset(2, 120, theCatches, theEfforts, "Catches", "Efforts", 10, 10, "test");
		// plot.reset(3, 120, theCatches, theEfforts, "Catches", "Efforts", 10, 10, "test");
		// plot.reset(4, 120, theCatches, theEfforts, "Catches", "Efforts", 10, 10, "test");
		plot.reset(5, 120, theStocks, theIncomes, "Stocks", "Income", 10, 10, "test");
		plot.repaint();
		}
	// ---------------------------------------------------------------------------
}
