package fr.ird.frames;

import java.awt.*;
import java.awt.image.*;
import java.awt.geom.*;

import javax.swing.*;

import fr.ird.strings.MESSAGES;


public class PLOT extends JPanel {
	static final long serialVersionUID = -1L;
	double[] sequence1;
	double[] sequence2;
	String l1, l2;
	double max1, max2;
	int type, month=0;
	Color bcg = new Color(240,240,240);
	String titre ="";
	boolean smoothed = false; 
	int dur = 120;
	// ---------------------------------------------------------------------------
	public PLOT(String id) {
		// this.setSize(500, 500);
		if(id.equals(MESSAGES.playersNames[1])) smoothed = true;
		sequence1 = new double[dur];
		sequence2 = new double[dur];
		for(int i = 0; i <  dur; i++) {
			sequence1[i]= 0.0;
			sequence2[i]= 0.0;
		}
	}
	// ---------------------------------------------------------------------------
	public void reset(int ty, int mo, double[] seq1, double[] seq2, String sl1, String sl2, int mx1, int mx2, String ti){
		type = ty;
		month = mo;
		l1 = sl1;
		l2 = sl2;
		max1 = (double)mx1;
		max2 = (double)mx2;
		for(int i = 0; i <  seq1.length; i++) {
			sequence1[i]= seq1[i];
			max1 = Math.max(max1, (int)(1.2f * sequence1[i]));
		}
		if(smoothed) {
			for(int i = 0; i <  seq1.length; i++) {
				sequence1[i]= seq1[12 * (i/12)];
			}			
		}
		else {
			for(int i = 0; i <  seq1.length; i++) {
				sequence1[i]= seq1[i];
			}
		}
		if(seq2 != null) for(int i = 0; i <  seq2.length; i++) {
			sequence2[i]= seq2[i];
			max2 = Math.max(max2, (int)(1.2f * sequence2[i]));
		}
		titre = ti;
	}
	// ---------------------------------------------------------------------------
	public void paint(Graphics g){
		int w = getWidth();
		int h = getHeight();
//		System.out.println("paint "+ w+ " "+ h + " type:"  + type + " month:"  +month);
		BufferedImage bi = new BufferedImage(w, h, BufferedImage.TYPE_INT_RGB);
		Graphics big = bi.createGraphics();
		Graphics2D big2 = (Graphics2D)big;
		big2.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
		big2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		big2.setColor(bcg);
		big2.fillRect(0,0,w,h);
		if(type==1) simplePlot(w,h,big2);
		if(type==2) biPlot(w,h,big2);
		if(type==3) plotShot(w,h,big2);
		if(type==4) simpleLine(w,h,big2);
		if(type==5) plotViab(w,h,big2);
		big2.setColor(Color.black);
		big2.setFont(new Font("SansSerif", Font.PLAIN, 12));
		int y = 1 + month/12;
		String [] mo = new String[]{"Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"};
		big2.drawString(mo[month%12]+" " + y, 6 * w/8, h/9);
		g.drawImage(bi,0,0, this);
	}
	// ---------------------------------------------------------------------------
	private int scaleH(int w, double x, double mx){
		return( (int)(0.1 * w + (0.8 * x * w) / mx) );
	}
	// ---------------------------------------------------------------------------
	private int scaleV(int h, double y, double my){
		return( (int)(0.9 * h - (0.8 * y * h) / my) );
	}
	// ---------------------------------------------------------------------------
	private void simpleLine(int w, int h, Graphics2D big){
		if(month>0){
			// ---------------------------------------
			int[] ix = new int[2 * month];
			int[] iy = new int[2 * month];
			for(int i = 0; i < month; i++){
				ix[2 * i] = scaleH(w,(double)i, (double)dur);
				iy[2 * i] = scaleV(h, sequence1[i], max1);
				ix[2 * i +1 ] = scaleH(w,i+1,dur);
				iy[2 * i + 1 ] = iy[2 * i] ;
				}
			// ---------------------------------------
			big.setStroke(new BasicStroke(1.0f));
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			path.moveTo(ix[0], iy[0]);
			for(int i = 0; i < 2 * month; i++) path.lineTo(ix[i], iy[i]);
	        big.setPaint(Color.red);
			big.draw(path);
	        xAxis( w,  9 * h/10,  big, l1, true);
			yAxis( w/10,  h,  big, l2, true);
			}
		}
	// ---------------------------------------------------------------------------
	private void simplePlot(int w, int h, Graphics2D big){
		if(month>0){
			// ---------------------------------------
			int[] ix = new int[2 * month+2];
			int[] iy = new int[2 * month+2];
			for(int i = 0; i < month; i++){
				ix[2 * i] = scaleH(w,i,dur);
				iy[2 * i] = scaleV(h, sequence1[i], max1);
				ix[2 * i +1 ] = scaleH(w,i+1,dur);
				iy[2 * i + 1 ] = iy[2 * i] ;
				}
			ix[2 * month]= ix[2 * month -1];
			iy[2 * month]=  9 * h /10;
			ix[2 * month + 1]= ix[0];
			iy[2 * month + 1]=  iy[2 * month];
			// ---------------------------------------
			big.setStroke(new BasicStroke(1.0f));
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			path.moveTo(ix[0], iy[0]);
			for(int i = 0; i < 2 * month + 2; i++) path.lineTo(ix[i], iy[i]);
			path.closePath();
	        big.setPaint(Color.cyan);
			big.fill(path);
	        big.setPaint(Color.black);
			big.draw(path);
	        xAxis( w,  9 * h/10,  big, "year", true);
			yAxis( w/10,  h,  big, l2, true);
			}
		}
	// ---------------------------------------------------------------------------
	private void biPlot(int w, int h, Graphics2D big){
		if(month>0){
        	// ---------------------------------------
        	int[] ix = new int[month];
			int[] iy = new int[month];
			for(int i = 0; i < month; i++){
				ix[i] = scaleH(w, (int)sequence1[i],max1);
				iy[i] = scaleV(h, (int)sequence2[i],max2);
				}
			// ---------------------------------------
        	big.setStroke(new BasicStroke(2.0f));
        	big.setPaint(Color.blue);
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			path.moveTo(ix[0], iy[0]);
			for(int i = 0; i < month ; i++) path.lineTo(ix[i], iy[i]);
			big.draw(path);
			big.setPaint(Color.black);
			big.setStroke(new BasicStroke(1.0f));
        	xAxis( w,  9 * h/10,  big, l1, true);
			yAxis( w/10,  h,  big, l2, true);
			// ---------------------------------------
			}
	}
	// ---------------------------------------------------------------------------
	private int mMM(int m, int i){
		return(Math.min(( 95 * m)/100, Math.max(( 5 * m )/100, i)));
	}
	// ---------------------------------------------------------------------------
	private void plotViab(int w, int h, Graphics2D big){
		if(month>0){
			// ---------------------------------------
	       	int[] ix = new int[month];
			int[] iy = new int[month];
			for(int i = 0; i < month; i++){
				ix[i] = scaleH(w, (int)(sequence1[i]), max1);
				iy[i] = scaleV(h, (int)(sequence2[i]), max2);
				}
			// ---------------------------------------
        	xAxis( w,  9 * h/10,  big, l1, true);
			yAxis( w/10,  h,  big, l2, true);
			// ---------------------------------------
			GeneralPath viabSet = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			viabSet.moveTo( 2 * w /10, h/10);
			viabSet.lineTo( 9 * w/ 10, h/10);
			viabSet.lineTo( 9 * w/ 10, 7 * h/10);
			viabSet.lineTo( 2 * w/ 10, 7 * h/10);
			viabSet.lineTo( 2 * w /10, h/10);
			big.setPaint(Color.yellow);
			big.setStroke(new BasicStroke(2.0f));
			big.fill(viabSet);
			// ---------------------------------------
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			path.moveTo(ix[month-6], iy[month-6]);
			for(int i = month-5; i < month ; i++) path.lineTo(ix[i], iy[i]);
			big.setPaint(Color.black);
			big.setStroke(new BasicStroke(2.0f));
			big.draw(path);
			big.setPaint(Color.green);
			big.setStroke(new BasicStroke(4.0f));
			if(month>6){
				for(int i = month-6; i < month; i++){
					big.setColor(Color.green);
					big.draw(new Ellipse2D.Float(ix[i]-5, iy[i]-5, 10, 10));
					}
				}
			// ---------------------------------------
			}
		}
	// ---------------------------------------------------------------------------
	private void plotShot(int w, int h, Graphics2D big){
		if(month>0){
			// ---------------------------------------
	       	int[] ix = new int[month];
			int[] iy = new int[month];
			for(int i = 0; i < month; i++){
				ix[i] = mMM(w,scaleH(w, (int)(sequence1[i]), (int)(2.0f * max1)));
				iy[i] = mMM(h,scaleV(h, (int)(sequence2[i]), (int)(2.0f * max2)));
				}
			// ---------------------------------------
			xAxis( w,  h/2,  big, l1, false);
			yAxis( w/2,  h,  big, l2, false);
			// ---------------------------------------
			big.setStroke(new BasicStroke(4.0f));
			int ew =  w / 10 ;
			int eh =  h / 10 ;
        	big.setPaint(Color.red);
			big.draw(new Ellipse2D.Float(w/2 - ew, h/2 - eh,  2 * ew, 2 * eh));
			big.draw(new Ellipse2D.Float(w/2 - 2 * ew, h/2 - 2 * eh, 4 * ew, 4 * eh));
			big.draw(new Ellipse2D.Float(w/2 - 3 * ew, h/2 - 3 * eh, 6 * ew, 6 * eh));
			big.draw(new Ellipse2D.Float(w/2 - 4 * ew, h/2 - 4 * eh, 8 * ew, 8 * eh));
			// ---------------------------------------
			GeneralPath path = new GeneralPath(GeneralPath.WIND_EVEN_ODD, ix.length);
			path.moveTo(ix[month-6], iy[month-6]);
			for(int i = month-5; i < month ; i++) path.lineTo(ix[i], iy[i]);
			big.setPaint(Color.black);
			big.setStroke(new BasicStroke(2.0f));
			big.draw(path);
			big.setPaint(Color.green);
			big.setStroke(new BasicStroke(4.0f));
			if(month>6){
				for(int i = month-6; i < month; i++){
					big.setColor(Color.green);
					big.draw(new Ellipse2D.Float(ix[i]-5, iy[i]-5, 10, 10));
					}
				}
			// ---------------------------------------
			}
		}
	// ---------------------------------------------------------------------------
	private void xAxis(int w, int ih, Graphics2D big,String axeLabel, boolean ticks){
		big.setStroke(new BasicStroke(1.0f));
    	big.setColor(Color.black);
		big.drawLine(w/10, ih, 95 * w/100, ih);
		big.setFont(new Font("SansSerif", Font.PLAIN, 10));
		big.drawString(axeLabel, 95 * w /10, ih - 8);	
		if(axeLabel.equals("year")){
			for(int y = 0; y <12; y++){
				int pw = scaleH(w,y,12);
				big.drawLine(pw, ih-2, pw, ih +2);
				}
			for(int y = 2; y <12; y+=2){
				int pw = scaleH(w,y,12);
				if(ticks) big.drawString(""+y, pw, ih + 10);
				}
			}
		else {
			for(int y = 1; y < 4; y++){
				int pw = scaleH(w,y,4);;
				big.drawLine(pw, ih-2, pw, ih + 2);
				if(ticks) big.drawString(""+(int)(y* max1/4), pw, ih + 10);
				}
			}
		}
	// ---------------------------------------------------------------------------
	private void yAxis(int iw, int h, Graphics2D big, String axeLabel, boolean ticks){
		big.setStroke(new BasicStroke(1.0f));
    	big.setColor(Color.black);
		big.drawLine(iw, h/20, iw, 9 * h /10);
		big.setFont(new Font("SansSerif", Font.PLAIN, 10));
		big.drawString(axeLabel, iw -15, h /20);
		for(int y = 1; y < 4; y++){
			int ph = scaleV(h,y,4);
			big.drawLine(iw-2,  ph, iw+2, ph);
			if(ticks && type==1) big.drawString(""+(int)(y * max1/4), iw - 30, ph);
			if(ticks && type==2) big.drawString(""+(int)(y * max2/4), iw - 30, ph);
			}
		}
	// ---------------------------------------------------------------------------
	}
