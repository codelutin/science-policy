package fr.ird.frames;

import java.util.*;
import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;

import fr.ird.communication.MESSAGE_MNGR;
import fr.ird.game.PLAYER;
import fr.ird.strings.DELIMITERS;
import fr.ird.strings.MESSAGES;


public class PLAYER_FRAME extends JFrame  {

	static final long serialVersionUID = -1L;

	JTabbedPane jTabbedPane;
	JPanel jPanel0, jPanel1, jPanel2;
	JList<String> jListMessages1, jListMessages2;
	JTextArea theChat1, theChat2;
	JPanel contentPane;
	int month;
	PLOT plotStates, plotActs, plotBi, plotShot, plotStocks;
	SLIDER pSlider;
	PLAYER player;
	// ---------------------------------------------------------------------------
	public PLAYER_FRAME(PLAYER pl) {    
		player = pl;
		try {
			Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
			setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
			setResizable(false);
			validate();
			int ww = (int)(0.6f * screenSize.width);
			int hh = (int)(0.6f * screenSize.height);
			setSize(ww,hh);
			setLocation((int)(0.2f * screenSize.width),(int)(0.2f * screenSize.height));
			initFrame();
		}
		catch (Exception exception) {
			exception.printStackTrace();
		}
	}
	// ---------------------------------------------------------------------------
	private void initFrame() throws Exception {
		UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
		contentPane = (JPanel) getContentPane();
		contentPane.setLayout(new GridLayout(1,3));
		jPanel0 = mkPanel0();
		jPanel1 = mkPanel1();
		jPanel2 = mkPanel2();
		setTitle(player.identity);
		this.add(jPanel0);
		this.add(jPanel1);
		this.add(jPanel2);
	}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel0(){
		plotActs = new PLOT(player.identity);
		plotStates = new PLOT(player.identity);
		plotBi = new PLOT(player.identity);
		plotShot = new PLOT(player.identity);
		plotStocks = new PLOT(player.identity);
		if(player.identity.equals(MESSAGES.playersNames[0])) return(mkPanel0Player0());
		if(player.identity.equals(MESSAGES.playersNames[1])) return(mkPanel0Player1());
		if(player.identity.equals(MESSAGES.playersNames[2])) return(mkPanel0Player2());
 		return(null);
 		}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel0Player0(){
		jPanel0 = new JPanel();
		jPanel0.setLayout(new GridBagLayout());
                GridBagConstraints gbc = new GridBagConstraints();
  		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5,5,5,5);
		gbc.weightx = 1.0f;
		gbc.weighty = 1.0f;
		jPanel0.add(plotStates,gbc);
		gbc.gridy = 1;
		jPanel0.add(plotActs,gbc);        
		gbc.gridy = 2;
		jPanel0.add(plotBi,gbc);        
		gbc.gridy = 3;
		jPanel0.add(plotStocks,gbc);        
		return(jPanel0);
	}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel0Player1(){
		jPanel0 = new JPanel();
		jPanel0.setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
  		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5,5,5,5);
		gbc.weightx = 1.0f;
		gbc.weighty = 1.0f;
		jPanel0.add(plotStates,gbc);
		gbc.gridy = 1;
		jPanel0.add(plotActs,gbc);        
		pSlider = new SLIDER("LIMITATION", "DEFINE", "Effort variation","%", -20.0f, 20.0f, 0.0f, this);
		gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridy = 2;
                gbc.weighty = 0.1f;
		jPanel0.add(pSlider,gbc);		
        return(jPanel0);
	}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel0Player2(){
		jPanel0 = new JPanel();
		jPanel0.setLayout(new GridBagLayout());
                GridBagConstraints gbc = new GridBagConstraints();
  		gbc.fill = GridBagConstraints.BOTH;
		gbc.gridx = 0;
		gbc.gridy = 0;
		gbc.insets = new Insets(5,5,5,5);
		gbc.weightx = 1.0f;
		gbc.weighty = 1.0f;
		jPanel0.add(plotShot,gbc);
		return(jPanel0);
	}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel1(){
		// panel to other player (1)
		theChat1 = new JTextArea("Chat");
		jListMessages1 = new JList<String>(player.listMessages1);
		jListMessages1.setSelectedIndex(0);
		jListMessages1.addListSelectionListener(new ListSelectionListener() {
			 public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting()) return;
				String msg = (String)jListMessages1.getSelectedValue();
				 player.sendMessage(msg,0);
           		}          
		 	}
    		);		
		return (dialogPanel(1,theChat1, jListMessages1));
		}
	// ---------------------------------------------------------------------------
	private JPanel mkPanel2(){
		// panel to other player (2)
		theChat2 = new JTextArea("Chat");
		jListMessages2 = new JList<String>(player.listMessages2);
		jListMessages2.setSelectedIndex(0);
		jListMessages2.addListSelectionListener(new ListSelectionListener() {
			 public void valueChanged(ListSelectionEvent evt) {
				if (evt.getValueIsAdjusting()) return;
				String msg = (String)jListMessages2.getSelectedValue();
				 player.sendMessage(msg,1);
          		}          
		 	}
   		);		
		return (dialogPanel(2,theChat2, jListMessages2));
		}
	// ---------------------------------------------------------------------------
	private JPanel dialogPanel(int nbp,JTextArea theChat, JList<String> jListMessages){
		theChat.setLineWrap(true);
		theChat.setWrapStyleWord(true);
		theChat.setRows(300);
		theChat.setText("Chat");
		theChat.setBackground(Color.white);
		theChat.setEditable(false);
		JScrollPane scrollerChat = new JScrollPane(theChat);
		jListMessages.setVisibleRowCount(3);
		JScrollPane scrollerList = new JScrollPane(jListMessages);
		JLabel title = new JLabel("Dialog with "+ player.otherPlayers[nbp-1]);
		
		JPanel topPanel = new JPanel();
		topPanel.setLayout(new GridLayout(2,1));
		topPanel.add(title);

		JPanel bottomPanel = new JPanel();
		bottomPanel.setLayout(new GridLayout(2,1));
		bottomPanel.add(scrollerList);
		bottomPanel.add(scrollerChat);

		JPanel jPanel = new JPanel();
		GridBagConstraints gbc = new GridBagConstraints();
                jPanel.setLayout(new GridBagLayout());
                gbc.fill = GridBagConstraints.HORIZONTAL;
                gbc.gridx = 0;
                gbc.gridy = 0;
                gbc.weightx = 1.0f;
                gbc.weighty = 0.1f;
		jPanel.add(topPanel, gbc);
		gbc.fill = GridBagConstraints.BOTH;
                gbc.gridx = 0;
                gbc.gridy = 1;
                gbc.weighty = 0.9f;
		jPanel.add(bottomPanel,gbc);
		return(jPanel);
	}
	// ---------------------------------------------------------------------------
	public void updateView(int month, double[] yield,double[]  effort, double [] temp, 
			double [] prices, double[] incomes, double[]  cpue, double[]  st,
			int maxY, int maxE, int maxS){
		plotStates.reset(1, month, yield, null, "year","Catches",  maxY, 0, "");
		plotStates.repaint();
		plotActs.reset(1, month, effort, null,  "year", "Effort", maxE, 0, "");
		plotActs.repaint();
		plotStocks.reset(1, month, st, null,  "year","Stock", maxS, 0, "Estimates");
		plotStocks.repaint();
		plotBi.reset(2, month, effort, cpue, "Effort","CPUE", maxE, maxS, "Dynamics");
		plotBi.repaint();
		plotShot.reset(3, month, effort, yield, "Effort", "Catches", player.EMSY,  player.YMSY, "Targetted MSY");
		plotShot.repaint();
	}
	// ---------------------------------------------------------------------------
	public void noPlayers(){
			theChat1.setText("Not enough players");
			theChat2.setText("Not enough players");
	}
	// ---------------------------------------------------------------------------
	public void updateMessages(String rMessage){
		Vector<String[]> vector = MESSAGE_MNGR.stringToVector(rMessage);
		StringBuffer sb1 = new StringBuffer("Chat"+DELIMITERS.seoln);
		StringBuffer sb2 = new StringBuffer("Chat"+DELIMITERS.seoln);
		for(int v=0; v<vector.size(); v++){
			String[] st = vector.elementAt(v);
			String from = st[0];
			String to = st[1];
			String msg = st[2];
			if(from.equals(player.identity) && to.equals(player.otherPlayers[0])) 
				sb1.append("To: "+msg+DELIMITERS.seoln);
			if(to.equals(player.identity) && from.equals(player.otherPlayers[0])) 
				sb1.append("From: "+msg+DELIMITERS.seoln);
			if(from.equals(player.identity) && to.equals(player.otherPlayers[1])) 
				sb2.append("To: "+msg+DELIMITERS.seoln);
			if(to.equals(player.identity) && from.equals(player.otherPlayers[1])) 
				sb2.append("From: "+msg+DELIMITERS.seoln);
		}
		theChat1.setText(sb1.toString());
		theChat2.setText(sb2.toString());
		}
	// ---------------------------------------------------------------------------
	public int getValues(){
            return ((int)pSlider.getValues());
            }
	// ---------------------------------------------------------------------------

	}
