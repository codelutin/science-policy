package fr.ird.game;

// import frames.PLAYER_FRAME;

public class PLAYER_HORLOGE extends Thread {
	static private PLAYER player;
	static private long time, timeInit;
	static int rap = 1;
	// ---------------------------------------------------------------------------
	public PLAYER_HORLOGE(PLAYER gpp ) { 
		player = gpp;
	}
	// ---------------------------------------------------------------------------
	static int secondes(){
		time = System.currentTimeMillis();
		return( (int)((time-timeInit)/1000));
	}
	// ---------------------------------------------------------------------------
	public static void reset() {
		timeInit = System.currentTimeMillis();
	}
	// ---------------------------------------------------------------------------
	public void run() {
		while(true){
			try {
				Thread.sleep(200);
				player.topHorloge();
			}
			catch (Exception e) { 
				e.printStackTrace();
				System.exit(0);
			}
		}
	}
	// ---------------------------------------------------------------------------
}
