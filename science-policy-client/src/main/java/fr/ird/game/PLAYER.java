package fr.ird.game;

import fr.ird.client.SERVLET_MESSAGE_HANDLER;
import fr.ird.communication.MESSAGE_MNGR;
import fr.ird.configuration.CONFIGURATION;
import fr.ird.frames.PLAYER_FRAME;
import fr.ird.model.OCEAN;
import fr.ird.strings.DELIMITERS;
import fr.ird.strings.MESSAGES;


public class PLAYER {
	// --------------------------------------------------------------------------------------
	PLAYER_FRAME pFrame;
	static SERVLET_MESSAGE_HANDLER messageHandler;
	public String identity;
	public String[] otherPlayers;
	public String[] listMessages1, listMessages2;
	public int YMSY = 0, EMSY;
	PLAYER_HORLOGE horloge;
	double[] theStocks, theCatches, theEfforts, theTemps, thePrices, theIncomes;
	int numPlay;
	int month = 0;

    private CONFIGURATION configuration = CONFIGURATION.INSTANCE;

	// --------------------------------------------------------------------------------------
	public PLAYER(SERVLET_MESSAGE_HANDLER handler) {
		// est cree au lancement de l'applet pca
		identity = "A";
		messageHandler = handler;
		String rMessage = send("CONNECT");
		String[] dMessage = MESSAGE_MNGR.parseMessage(rMessage);
		if(dMessage[0].equals("IDENTITY")) {
			makeIdentity(dMessage);
			pFrame = new PLAYER_FRAME(this);
			pFrame.setVisible(true);
			startHorloge();
			}
		else System.exit(0);
	}
	//	--------------------------------------------------------------------------------------
	public String send(String message){
		return(messageHandler.sendMessage(message));
		};
	//	--------------------------------------------------------------------------------------
	public String debMessage(String typeMessage){
			return(
					""+numPlay 
					+ DELIMITERS.seoln + identity
					+ DELIMITERS.seoln + typeMessage
					);
			};
	//	--------------------------------------------------------------------------------------
	private void makeIdentity(String[] dMessage){
		numPlay = Integer.parseInt(dMessage[1]);
		identity = dMessage[2];
		if(configuration.isVerbose()) System.out.println("ident "+identity+ "  "+numPlay);
		if(identity.equals(MESSAGES.playersNames[0])){
			otherPlayers = new String[]{
					MESSAGES.playersNames[1],
					MESSAGES.playersNames[2],
			};
			listMessages1 = MESSAGES.gMessagesAB;
			listMessages2 = MESSAGES.gMessagesAC;
		}
		if(identity.equals(MESSAGES.playersNames[1])){
			otherPlayers = new String[]{
					MESSAGES.playersNames[2],
					MESSAGES.playersNames[0],
			};
			listMessages1 = MESSAGES.gMessagesBC;
			listMessages2 = MESSAGES.gMessagesBA;
		}
		if(identity.equals(MESSAGES.playersNames[2])){
			otherPlayers = new String[]{
					MESSAGES.playersNames[0],
					MESSAGES.playersNames[1],
			};
			listMessages1 = MESSAGES.gMessagesCA;
			listMessages2 = MESSAGES.gMessagesCB;
			EMSY = Integer.parseInt(dMessage[3]);
			YMSY = Integer.parseInt(dMessage[4]);
		}
	}
	// --------------------------------------------------------------------------------------
	private void startHorloge(){
		horloge = new PLAYER_HORLOGE(this);
		try{
			horloge.start();
			}
		catch(Exception e){
			e.printStackTrace();
			}
		}
	//	--------------------------------------------------------------------------------------
	public String sendMessage(String msg, int other){
		return(debMessage("MSG)"
     			 + DELIMITERS.seoln + otherPlayers[other]
             	 + DELIMITERS.seoln + msg));
	}
	// ---------------------------------------------------------------------------
	public void topHorloge(){
		getView();
		if(identity.equals(MESSAGES.playersNames[1])){
			send(debMessage("ACT")
					+ DELIMITERS.seoln + (int)pFrame.getValues());	
			}
		getMessages();
		}
	// ---------------------------------------------------------------------------
	public void getMessages(){
		String rMessage = send(debMessage("MSG?"));	
		if(rMessage.equals("NOT_RUNNING")){
			pFrame.noPlayers();
			}
		else {
			pFrame.updateMessages(rMessage);
			}
		}
	//	--------------------------------------------------------------------------------------
	/*
		for(int m=0;m< nbMonths; m++){
			seq[m]= theCatches[m];
			seq[m+nbMonths]= theEfforts[m];
			seq[m+2*nbMonths]= theTemps[m];
			seq[m+3*nbMonths]= thePrices[m];
			seq[m+4*nbMonths]= theIncomes[m];
		}
		seq[5*nbMonths]= getQ();
		seq[5*nbMonths+1]= getMaxY();
		seq[5*nbMonths+2]= getMaxE();
		seq[5*nbMonths+3]= getMaxS();
    */
	public void getView(){
		int nbMonths = 120;
		theStocks =  new double [nbMonths];
		theCatches =  new double [nbMonths];
		theEfforts =  new double [nbMonths];
		theTemps =  new double [nbMonths];
		thePrices =  new double [nbMonths];
		theIncomes =  new double [nbMonths];
		for(int m = 0; m<nbMonths; m++) {
			theStocks[m]= 0.0f;
			theCatches[m]= 0.0f;
			theEfforts[m]= 0.0f;
			theTemps[m] = 0.0f;
			thePrices[m] =  0.0f;
			theIncomes[m] =  0.0f;
			}
		String rMessage = send(debMessage("VIEW?")); 
		int mo = (int)Integer.parseInt(MESSAGE_MNGR.first(rMessage));
		double[] seq = MESSAGE_MNGR.stringToList(rMessage);
		for(int m=0; m<nbMonths;m++){
			theCatches[m] = seq[m];
			theEfforts[m] = seq[m+nbMonths];
			theTemps[m] = seq[m+2*nbMonths];
			thePrices[m] = seq[m+3*nbMonths];
			theIncomes[m] = seq[m+4* nbMonths];
		}
		double q = seq[5*nbMonths];
		int maxY = (int)seq[5*nbMonths+1];
		int maxE = (int)seq[5*nbMonths+2];
		int maxS = (int)seq[5*nbMonths+3];
		// -----------------------
		double [] cpue = new double[nbMonths];
		double [] st = new double[nbMonths];
		for(int m=month;m<mo;m++) {
			cpue[m] = theCatches[m]/(q * (theEfforts[m]+0.0000001f));
			st[m] = theEfforts[m] * OCEAN.mRnd(0.3);
		}
		month = mo;
		pFrame.updateView(month, theCatches, theEfforts, theTemps, thePrices, theIncomes, cpue, st, maxY, maxE, maxS);
		}
	//	--------------------------------------------------------------------------------------
}

