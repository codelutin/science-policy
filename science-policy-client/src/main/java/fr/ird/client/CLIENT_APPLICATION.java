package fr.ird.client;

import fr.ird.game.PLAYER;

public class CLIENT_APPLICATION {

	public static void main(String[] args) {
		// On cree un nouveau joueur et on lui associe un message handler pour la gestion
		// de la communication avec la servlet.
		PLAYER player = new PLAYER(new SERVLET_MESSAGE_HANDLER());
	}

}
