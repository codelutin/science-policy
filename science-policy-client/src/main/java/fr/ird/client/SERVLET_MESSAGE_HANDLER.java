package fr.ird.client;

import java.io.*;
import java.net.*;

import fr.ird.communication.MESSAGE_MNGR;
import fr.ird.configuration.CONFIGURATION;

public class SERVLET_MESSAGE_HANDLER {
    // ----------------------------------------------------------------------------
    // lsof -i @localhost:8080
    // sudo kill -9 <<PID>>
    // ----------------------------------------------------------------------------
    static final long serialVersionUID = -1L;
    public static CONFIGURATION configuration = CONFIGURATION.INSTANCE;

    // -----------------------------------------------------------------------------
    public static URL urlServlet() throws MalformedURLException, IOException{
        return new URL(
            configuration.getProtocol(),
            configuration.getDomain(),
            Integer.parseInt(configuration.getPort()),
            "/science-policy-server/SERVLET_GAME"
        );
    }
    // ----------------------------
    private URLConnection getServletConnection() throws MalformedURLException, IOException {
        URL    urlServlet = urlServlet();
        if(configuration.isVerbose()) System.out.println("url " + urlServlet);
        URLConnection con = urlServlet.openConnection();
        if(configuration.isVerbose()) System.out.println("con " + con);
        con.setDoInput(true);
        con.setDoOutput(true);
        con.setUseCaches(false);
        con.setReadTimeout(0);
        con.setRequestProperty("Content-Type","application/x-java-serialized-object");
        return con;
    }

    // ---------------------------------------------------------------------------
    public String sendMessage(String smessage){
        String cmessage = MESSAGE_MNGR.cod(smessage);
        String rmessage = "";
        try {
            if(configuration.isVerbose()) System.out.println("PLAYER APPLET SEND MESSAGE  :      " +cmessage);
            URLConnection con = getServletConnection();
            if(configuration.isVerbose()) System.out.println("con " + con);
            OutputStream outstream = con.getOutputStream();
            if(configuration.isVerbose()) System.out.println("out " + outstream);
            ObjectOutputStream oos = new ObjectOutputStream(outstream);
            if(configuration.isVerbose()) System.out.println("oos " + oos);
            oos.writeObject(cmessage);
            oos.flush();
            oos.close();
            if(configuration.isVerbose()) System.out.println("oos " + oos);
            InputStream instr = con.getInputStream();
            if(configuration.isVerbose()) System.out.println("ins " + instr);
            ObjectInputStream inputFromServlet = new ObjectInputStream(instr);
            rmessage = (String) inputFromServlet.readObject();
            if(configuration.isVerbose()) System.out.println("RECEIVED FROM SERVLET  :       " + rmessage);
            inputFromServlet.close();
            instr.close();
        } catch (Exception ex) {
            ex.printStackTrace();
            System.exit(0);
        }
        return(MESSAGE_MNGR.decod(rmessage));
    }
    // ---------------------------------------------------------------------------
}
